# install selenium version 4.14.0 through pip
# install webdriver-manager version 4.0.1 through pip
# install chrome on linux using sudo apt

import time
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver import ChromeOptions
from webdriver_manager.chrome import ChromeDriverManager


class FAATest(unittest.TestCase):
    
    def setUp(self) -> None:
        options = ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--window-size=1920,1080")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--no-sandbox")
        self.driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()),
                                       options=options)
        self.driver.get("https://flint-aqua-advocates.me")
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

    def test_open_landing(self) -> None:
        driver = self.driver
        self.assertIn("Flint Aqua Advocates", driver.title)
        
    def test_open_about(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "About").click()
        title = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(title.text, "About Page:")
            
    def test_open_restaurants(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Restaurants").click()
        title = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(title.text, "Restaurants")
        
    def test_open_neighborhoods(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Neighborhoods").click()
        title = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(title.text, "Neighborhoods")
            
    def test_open_water_providers(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Water Providers").click()
        title = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(title.text, "Water Providers")

    def test_navigate_to_restaurant_instance(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Restaurants").click()
        card = driver.find_element(By.ID, "instance-card")
        card.click()
        instance = driver.find_element(By.ID, "restaurant")
        self.assertIsNotNone(instance)
        
    def test_navigate_to_water_provider_instance(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Water Providers").click()
        card = driver.find_element(By.ID, "instance-card")
        card.click()
        instance = driver.find_element(By.ID, "water-provider")
        self.assertIsNotNone(instance)

    def test_navigate_to_neighborhood_instance(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Neighborhoods").click()
        card = driver.find_element(By.ID, "instance-card")
        card.click()
        instance = driver.find_element(By.ID, "neighborhood")
        self.assertIsNotNone(instance)

    def test_navigate_to_neighborhood_from_restaurant(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Restaurants").click()
        card = driver.find_element(By.ID, "instance-card")
        card.click()
        driver.find_element(By.LINK_TEXT, "Neighborhoods").click()
        card = driver.find_element(By.ID, "instance-card")
        card.click()
        instance = driver.find_element(By.ID, "neighborhood")
        self.assertIsNotNone(instance)

    def test_navigate_to_water_provider_from_restaurant(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Restaurants").click()
        card = driver.find_element(By.ID, "instance-card")
        card.click()
        driver.find_element(By.LINK_TEXT, "Water Providers").click()
        card = driver.find_element(By.ID, "instance-card")
        card.click()
        instance = driver.find_element(By.ID, "water-provider")
        self.assertIsNotNone(instance)

        
    def tearDown(self) -> None:
        self.driver.close()

if __name__ == "__main__":
    unittest.main()