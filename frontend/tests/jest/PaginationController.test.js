import "@testing-library/jest-dom";
import { expect, jest } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import PaginationController from "../../src/components/PaginationController";

const setPage = jest.fn();

it("renders without error", () => {
  const totalPages = 10;
  render(<PaginationController totalPages={totalPages} setPage={setPage} />);

  expect(screen.getByText("Total Pages: " + totalPages)).toBeInTheDocument();
  expect(screen.getByText("Previous")).toBeInTheDocument();
  expect(screen.getByText("Next")).toBeInTheDocument();
});
