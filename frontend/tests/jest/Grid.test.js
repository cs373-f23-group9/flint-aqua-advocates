import "@testing-library/jest-dom";
import { expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import Grid from "../../src/components/Grid";

it("renders without errors", () => {
  const elements = [3];
  render(<Grid gridElements={elements} numCols={10} />);

  expect(screen.getByTestId("grid")).toBeInTheDocument();
});

it("renders correct number of cards", () => {
  const elements = Array.from({ length: 15 }, () => <h1>test-card</h1>);
  render(<Grid gridElements={elements} numCols={5} />);

  const renderedElements = screen.getAllByText("test-card");
  expect(renderedElements.length).toBe(15);
});
