import "@testing-library/jest-dom";
import { expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import MemberCard from "../../src/components/MemberCard";

it("renders card data correctly", () => {
  const member = {
    name: "test name",
    role: "test role",
    bio: "test bio",
    gitlab_id: "test gitlab id",
  };
  const numCommits = 5;
  const numIssues = 10;
  render(
    <MemberCard
      member={member}
      numCommits={numCommits}
      numIssues={numIssues}
    />,
  );

  expect(screen.getByText(/.*test name$/)).toBeInTheDocument();
  expect(screen.getByText(/.*test role$/)).toBeInTheDocument();
  expect(screen.getByText(/.*test bio$/)).toBeInTheDocument();
  expect(screen.getByText(/.*test gitlab id$/)).toBeInTheDocument();
  expect(screen.getByText(/.*5$/)).toBeInTheDocument();
  expect(screen.getByText(/.*10$/)).toBeInTheDocument();
});
