import "@testing-library/jest-dom";
import { expect, jest } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import "react-router-dom";
import * as router from "react-router";
import Neighborhoods from "../../src/components/Neighborhoods";

const navigate = jest.fn();
beforeEach(() => {
  jest.spyOn(router, "useNavigate").mockImplementation(() => navigate);
});

it("renders without errors", () => {
  render(<Neighborhoods />);
  // Check that it rendered properly
  const elements = screen.getAllByText("Neighborhoods");
  expect(elements.length).toBeGreaterThanOrEqual(1);

  const regex = /Found \d+ neighborhoods/;
  const element = screen.getByRole("heading", { name: regex });
  expect(element).toBeInTheDocument();

  // check grid rendered
  expect(screen.getByTestId("grid")).toBeInTheDocument();
});
