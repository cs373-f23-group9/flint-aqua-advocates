import "@testing-library/jest-dom";
import { expect, jest } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import Restaurants from "../../src/components/Restaurants";
import "react-router-dom";
import * as router from "react-router";

const navigate = jest.fn();
beforeEach(() => {
  jest.spyOn(router, "useNavigate").mockImplementation(() => navigate);
});

it("renders without errors", () => {
  render(<Restaurants />);
  // Check that it rendered properly
  const elements = screen.getAllByText("Restaurants");
  expect(elements.length).toBeGreaterThanOrEqual(1);

  const regex = /Found \d+ restaurants/;
  const element = screen.getByRole("heading", { name: regex });
  expect(element).toBeInTheDocument();

  // check grid rendered
  expect(screen.getByTestId("grid")).toBeInTheDocument();
});
