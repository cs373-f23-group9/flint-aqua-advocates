import "@testing-library/jest-dom";
import { expect, jest } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import "react-router-dom";
import * as router from "react-router";
import WaterProviders from "../../src/components/WaterProviders";

const navigate = jest.fn();
beforeEach(() => {
  jest.spyOn(router, "useNavigate").mockImplementation(() => navigate);
});

it("renders without errors", () => {
  render(<WaterProviders />);
  // Check that it rendered properly
  const elements = screen.getAllByText("Water Providers");
  expect(elements.length).toBeGreaterThanOrEqual(1);

  const regex = /Found \d+ water providers/;
  const element = screen.getByRole("heading", { name: regex });
  expect(element).toBeInTheDocument();

  // check grid rendered
  expect(screen.getByTestId("grid")).toBeInTheDocument();
});
