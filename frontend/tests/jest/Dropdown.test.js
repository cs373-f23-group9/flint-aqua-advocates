import "@testing-library/jest-dom";
import { expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import Dropdown from "../../src/components/Dropdown";

it("renders dropdown options correctly", () => {
  const options = ["opt1", "opt2"];
  render(<Dropdown dropdownElements={options} />);

  expect(screen.getByText("opt1")).toBeInTheDocument();
  expect(screen.getByText("opt2")).toBeInTheDocument();
});
