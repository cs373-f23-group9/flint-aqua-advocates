import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import NavBar from "../../src/components/Navbar";

it("renders without errors", () => {
  render(<NavBar />);
  // Check that all elements are present
  expect(screen.getByText("FAA")).toBeInTheDocument();
  expect(screen.getByText("Home")).toBeInTheDocument();
  expect(screen.getByText("About")).toBeInTheDocument();
  expect(screen.getByText("Water Providers")).toBeInTheDocument();
  expect(screen.getByText("Neighborhoods")).toBeInTheDocument();
  expect(screen.getByText("Restaurants")).toBeInTheDocument();
});
