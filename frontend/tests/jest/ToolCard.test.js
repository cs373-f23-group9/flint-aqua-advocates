import "@testing-library/jest-dom";
import { expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import ToolCard from "../../src/components/ToolCard";

it("renders card data correctly", () => {
  const name = "test name";
  const description = "test description";
  render(<ToolCard description={description} name={name} />);

  expect(screen.getByText(/.*test name$/)).toBeInTheDocument();
  expect(screen.getByText(/.*test description$/)).toBeInTheDocument();
});
