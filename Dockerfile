# Use the micromamba Docker image
FROM mambaorg/micromamba:1.5.1

# Copy the environment.yml to the temporary directory in Docker
COPY environment.yml /tmp/environment.yml

# Install the environment and perform cleanup
RUN micromamba install -y -n base -f /tmp/environment.yml && \
    micromamba clean --all --yes

# Ensure Conda environment is activated for RUN commands
ARG MAMBA_DOCKERFILE_ACTIVATE=1 

ENV PATH="/usr/bin:${PATH}"

CMD bash