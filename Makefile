# build the website
build:
	CI=false && cd ./frontend && npm ci && npm run build

# Remove build files
clean:
	rm -rf  ./frontend/build
	rm -rf ./frontend/node_modules
	rm -rf ./backend/__pycache__

test_jest:
	cd ./frontend && npm install && npm run test

test_selenium:
	pip install selenium webdriver-manager
	wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
	sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
	apt-get update 
	apt-get -y install google-chrome-stable
	python3 ./frontend/tests/selenium-tests.py

test_unittest:
	python3 ./backend/backend_tests.py

test_api:
	npm install -g newman
	newman run ./backend/postman_collection.json