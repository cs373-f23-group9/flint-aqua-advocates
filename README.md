# Flint Aqua Advocates
- Website link: https://www.flint-aqua-advocates.me/
- Backend API: api.flint-aqua-advocates.me
- API Documentation: https://documenter.getpostman.com/view/29779387/2s9YJZ3jMM
- git SHA: 345f21ab65bcbc3f4d84f83dd0c129f6f6c49013
## Canvas/Ed Discussion Group 9

## Team Members

- Wenhan Samuel Lee
- Matthew Lin
- Ricardo Palma
- Cooper Ward
- Jeff Zhao

## Phase Leader
- Cooper Ward

### Responsibilities
The phase leader was responsible for putting thought towards the division of the project into mangable and taskable issues, as well as the actual assignment of those issue to individual group members. They were also responsible for organizing group meetings/check-in times and making sure that the group was on pace to hit it's deadlines.

## Name of Project
flint-aqua-advocates

## About Project
We will build a database for the residents of Flint, Michigan that displays information
regarding water providers, neighborhoods, and restaurants in and around the city. The 2014 
Flint water crisis has made it much harder for residents to access clean drinking water due 
to lead contamination. In this website, one can check which water providers, neighborhoods,
and restaurants are nearby each other so that they can find places that suit their needs.

## URLs of Sources

- https://www.epa.gov/enviro/sdwis-model
- https://www.attomdata.com/solutions/property-data-api
- https://rapidapi.com/apidojo/api/travel-advisor
- https://developers.google.com/maps/documentation/geocoding/overview

## Models

 - Water Sources
 - Neighborhoods
 - Restaurants

## Number of Instances per Model

- ~1300 Water Sources
- ~20 Neighborhoods
- ~350 Restaurants

## Attributes per Model
1. Water Sources
    - Population Served
    - Geographical Location
    - Zip Code
    - Phone Number
    - Email
2. Neighborhoods
    - Population number
    - Average Household Income
    - Median Rent
    - Total Housing Units
    - Available Housing Units
3. Restaurants
    - Address
    - Cuisine
    - Dietary restrictions
    - Rating
    - Phone

## Connections between Instances

- Water Sources
    * Nearby neighborhoods (i.e. within 5 miles)
    * Nearby restaurants
- Neighborhoods
    * Nearby water providers
    * Nearby restaurants
- Restaurants
    * Nearby water providers
    * Nearby neighborhoods

## Media in each Instance
- Water Sources
    * Map
    * Text
- Neighborhoods
    * Map
    * Text
    * Images
- Restaurant
    * Map
    * Text
    * Images

## Three Questions our Site will Answer

1. What water providers are present in and nearby your neighborhood?
2. What good restaurants are nearby your neighborhood?
3. What good restaurants are nearby water providers so that you can find good food and water?

## Work Time
- Estimated Time: 40 hours total
-  Actual Time: 55 hours total
