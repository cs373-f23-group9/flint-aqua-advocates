from flask import Flask, jsonify

from flask_cors import CORS

import git_metrics

app = Flask(__name__)
CORS(app)

@app.route('/api/statistics', methods=['GET'])
def get_statistics():
    # Replace with your actual logic to get statistics
    statistics = git_metrics.get_stats()
    
    return jsonify(statistics)

if __name__ == '__main__':
    app.run(port=5000)