import requests
import dotenv
import os

dotenv.load_dotenv()

class Developer:
    def __init__(self, name="default", commits=0, issues=0) -> None:
        self.name = name
        self.commit_num = commits
        self.issues = issues

        
GITLAB_URL = "https://gitlab.com"  # Replace with your GitLab instance URL
PROJECT_ID = "50467702"  # Replace with your project ID
PRIVATE_TOKEN = os.getenv("GITLAB_PERSONAL_ACCESS_TOKEN")  # Replace with your private token

COMMITS_URL = f"{GITLAB_URL}/api/v4/projects/{PROJECT_ID}/repository/commits"
ISSUES_URL = f"{GITLAB_URL}/api/v4/projects/{PROJECT_ID}/issues"
HEADERS = {"PRIVATE-TOKEN": PRIVATE_TOKEN}





def get_user_id_by_username(username):
        url = f"{GITLAB_URL}/api/v4/users"
        params = {"username": username}
        
        response = requests.get(url, headers=HEADERS, params=params)
        
        if response.status_code == 200:
            users = response.json()
            # Since we are searching by username, there should be at most one match
            if users:
                return users[0]['id']
        else:
            print(f"Failed to retrieve user by username. Response code: {response.status_code}")
            print(f"Response content: {response.content}")


def commit_update(email_to_developer):
    params = {"per_page": 100, "all": True, "all": True, "scope": "all"}  # 100 is the maximum per_page value

    #commits loop
    #uses emails to idntify
    page = 1
    while True:
        params["page"] = page
        response = requests.get(COMMITS_URL, headers=HEADERS, params=params)

        if response.status_code == 200:
            commits = response.json()
            if not commits:  # Break the loop if there are no more commits
                break
            for commit in commits:
                email_to_developer[commit['author_email']].commit_num += 1
            page += 1  # Go to the next page
        else:
            print(f"Failed to retrieve commits. Response code: {response.status_code}")
            print(f"Response content: {response.content}")
            break 

def issue_update(handle_to_developer):
    params = {"per_page": 100, "scope": "all"}  # 100 is the maximum per_page value

    #issues loop
    page = 1
    while True:
        params["page"] = page
        response = requests.get(ISSUES_URL, headers=HEADERS, params=params)

        if response.status_code == 200:
            issues = response.json()
            if not issues:  # Break the loop if there are no more issues
                break
            for issue in issues:
                author_name = issue['author']['username']
                dev_obj = handle_to_developer.get(author_name)
                if dev_obj is not None:
                    dev_obj.issues += 1
            page += 1  # Go to the next page
        else:
            print(f"Failed to retrieve issues. Response code: {response.status_code}")
            print(f"Response content: {response.content}")
            break

def get_stats():
    # Replace these variables with actual values
    email_to_developer = {}
    handle_to_developer = {}
    #key shoud be author name
    dev = Developer("Jeff Zhao")
    email_to_developer["jeffzhao100@gmail.com"] = dev
    handle_to_developer["jeffzhao100"] = dev

    dev = Developer("Cooper Ward")
    email_to_developer["cward3322@gmail.com"] = dev
    email_to_developer["cward3322@cs.utexas.edu"] = dev
    handle_to_developer["cward3322"] = dev

    dev = Developer("Wenhan Lee")
    email_to_developer["wenhan_lee@utexas.edu"] = dev
    email_to_developer["wenhan.s.lee@gmail.com"] = dev
    handle_to_developer["wleethecoder"] = dev

    dev = Developer("Matthew Lin")
    email_to_developer["mlin2778@gmail.com"] = dev
    handle_to_developer["mlin2778"] = dev

    dev = Developer("Ricardo Palma")
    email_to_developer["ricardo.palma.rjp@gmail.com"] = dev
    handle_to_developer["palma-ricardo"] = dev
        

    commit_update(email_to_developer)
    issue_update(handle_to_developer)    

    statistics = {}
    for dev_it in email_to_developer.values():
        print("name: ", dev_it.name)
        print("commit ", dev_it.commit_num)
        print("issue ", dev_it.issues)
        statistics[dev_it.name] = {"commits": dev_it.commit_num, "issues": dev_it.issues}

    return statistics


print(get_stats())