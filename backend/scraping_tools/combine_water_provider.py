from sqlalchemy import create_engine, Column, Integer, String, Date, Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import text

# Configuration
schema = "flint_aqua_advocates_db"
host = "database-test1.cgyts2xn7st2.us-east-1.rds.amazonaws.com"
user = "admin"
password = "frickYoumyhomeboy!"
port = 3306

SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{user}:{password}@{host}:{port}/{schema}'


# Create an SQLAlchemy engine to connect to your database
engine = create_engine(SQLALCHEMY_DATABASE_URI)

# Create a session to interact with the database
Session = sessionmaker(bind=engine)
session = Session()

# Define SQLAlchemy models for your tables
Base = declarative_base()

class WaterProvider(Base):
    __tablename__ = 'water_providers'

    # Primary Key
    pwsid = Column(String(255), primary_key=True)

    # Columns
    pws_name = Column(String, nullable=False)
    population_served_count = Column(Integer)
    address_line1 = Column(String)
    address_line2 = Column(String)
    zip_code = Column(String)
    pws_deactivation_date = Column(Date)
    season_begin_date = Column(Date)
    season_end_date = Column(Date)
    email_addr = Column(String)
    phone_number = Column(String)

    def __repr__(self):
        return f"<WaterProvider(pwsid={self.pwsid}, pws_name='{self.pws_name}')>"

class WaterProviderAndContaminant(Base):
    __tablename__ = 'water_providers_and_contaminants'

    # Primary Key
    pwsId = Column(String(255), primary_key=True)

    # Columns
    geo = Column(String)
    temporal = Column(Date)
    dataValueNitrates = Column(Float)
    categoryNitrates = Column(String)
    dataValueRadium = Column(Float)
    categoryRadium = Column(String)
    dataValueDisinfectionByproducts = Column(Float)
    categoryDisinfectionByproducts = Column(String)

    def __repr__(self):
        return f"<WaterProviderAndContaminant(pwsId={self.pwsId}, geo='{self.geo}', temporal='{self.temporal}')>"
    
    
class ResultTable(Base):
    __tablename__ = 'result_table'

    # Define columns based on the query result
    pwsId = Column(String(255), primary_key=True)
    pwsName = Column(String(255))
    populationServed = Column(Integer)
    address_line1 = Column(String(255))
    address_line2 = Column(String(255))
    zip_code = Column(String(255))
    pws_deactivation_date = Column(Date)
    season_begin_date = Column(Date)
    season_end_date = Column(Date)
    email_addr = Column(String(255))
    phone_number = Column(String(255))
    geo = Column(String(255))
    temporal = Column(Date)
    dataValueNitrates = Column(Float)
    categoryNitrates = Column(String(255))
    dataValueRadium = Column(Float)
    categoryRadium = Column(String(255))
    dataValueDisinfectionByproducts = Column(Float)
    categoryDisinfectionByproducts = Column(String(255))

Base.metadata.create_all(engine)


# Execute the query and store the result in the new table
data_query = text("""             
    SELECT
        wp.pwsid AS pwsId,
        wp.pws_name AS pwsName,
        wp.population_served_count AS populationServed,
        wp.address_line1,
        wp.address_line2,
        wp.zip_code,
        wp.pws_deactivation_date,
        wp.season_begin_date,
        wp.season_end_date,
        wp.email_addr,
        wp.phone_number,
        wpc.geo,
        wpc.temporal,
        wpc.dataValueNitrates,
        wpc.categoryNitrates,
        wpc.dataValueRadium,
        wpc.categoryRadium,
        wpc.dataValueDisinfectionByproducts,
        wpc.categoryDisinfectionByproducts
    FROM
        water_providers wp
    JOIN
        water_providers_and_contaminants wpc
    ON
        wp.pwsid = wpc.pwsId
""")

data = session.execute(data_query).fetchall()
#print(data[0])

# Transform data into a list of dictionaries
entries = [{
    "pwsId": row[0],
    "pwsName": row[1],
    "populationServed": row[2],
    "address_line1": row[3],
    "address_line2": row[4],
    "zip_code": row[5],
    "pws_deactivation_date": row[6],
    "season_begin_date": row[7],
    "season_end_date": row[8],
    "email_addr": row[9],
    "phone_number": row[10],
    "geo": row[11],
    "temporal": row[12],
    "dataValueNitrates": row[13],
    "categoryNitrates": row[14],
    "dataValueRadium": row[15],
    "categoryRadium": row[16],
    "dataValueDisinfectionByproducts": row[17],
    "categoryDisinfectionByproducts": row[18]
} for row in data]

# Use bulk_insert_mappings for optimized insertion
session.bulk_insert_mappings(ResultTable, entries)
session.commit()