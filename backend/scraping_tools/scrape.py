import requests
import pandas as pd
import sqlalchemy as db
import pymysql
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import time
import pickle

# pymysql.connect(host='database-test1.cgyts2xn7st2.us-east-1.rds.amazonaws.com', user='admin', password='####')

# This script was run only manually several times to scrape data from databases and write it to a MySQL database. It will not be run again.

# This method scrapes water provider data from the EPA database.
def get_provider_df(attributes, name) -> tuple[pd.DataFrame, str]:
    # the data source can return at most 10000 rows at a time.
    data_dict = dict()
    for a in attributes:
        data_dict[a] = []
    cities_within_genesee_county = [
        'flint',
        'grand blanc',
        'burton',
        'linden',
        'swartz creek',
        'davison',
        'fenton',
        'clio',
        'morris',
        'otisville',
        'atlas',
        'byron',
        'gaines',
        'montrose',
        'clayton',
        'flushing',
        'vienna',
        'genesee',
        'goodrich',
        'argentine'
    ]
    for city in cities_within_genesee_county:
        # response = requests.request("GET", f"https://data.epa.gov/efservice/water_system/state_code/=/MI/city_name/=/{city}/count/json")
        # json_data = response.json()
        # print(f"{city}: {json_data[0]['TOTALQUERYRESULTS']}")
        response = requests.request("GET", f"https://data.epa.gov/efservice/water_system/state_code/=/MI/city_name/=/{city}/json")

        json_data = response.json()
        for provider in json_data:
            # print(provider)
            for a in attributes:
                data_dict[a].append(provider[a])

    df = pd.DataFrame(data_dict)
    print(df)
    return (df, name)

provider_attributes = [
    'pwsid',
    'pws_name', 
    'population_served_count', 
    'address_line1', 
    'address_line2', 
    'zip_code', 
    'pws_deactivation_date', 
    'season_begin_date', 
    'season_end_date', 
    'email_addr', 
    'phone_number'
]
provider_database_name = 'water_providers'

# This method scrapes neighborhood data from the ATTOM database.
neighborhood_geo_id_list_url = "https://api.gateway.attomdata.com/v4/area/geoid/lookup/?geoId=CO26049&GeoType=ND"
neighborhood_headers = {
'apikey': '####'
}
def get_neighborhood_geo_id_list():
    response = requests.request("GET", neighborhood_geo_id_list_url, headers=neighborhood_headers)
    json_data = response.json()
    neighborhood_geo_id_list = []
    for neighborhood in json_data['response']['result']['package']['item']:
        neighborhood_geo_id_list.append(neighborhood['geoIdV4'])

    print(neighborhood_geo_id_list)
    print('Number of neighborhoods in Genesee County:', len(neighborhood_geo_id_list))

neighborhood_geography_attributes = [
    'geographyName',
    'longitude',
    'latitude'
]
neighborhood_demographics_attributes = [
    'population',
    'population_Urban_Pct',
    'population_Rural_Pct',
    'population_White_Pct',
    'population_Black_Pct',
    'population_Asian_Pct',
    'population_American_Indian_Or_Alaskan_Native_Pct',
    'housing_Units',
    'housing_Units_Vacant',
    'housing_Median_Rent',
    'avg_Household_Income',
    'costIndex_Annual_Expenditures',
    'costIndex_Food',
    'costIndex_Non_Alcoholic_Beverages',
    'costIndex_Local_Restaurants',
    'costIndex_Alcoholic_Beverages',
    'costIndex_Housing',
    'costIndex_Gasoline_And_Motor_Oil',
    'costIndex_Public_Transportation',
    'costIndex_Healthcare',
    'costIndex_Health_Insurance',
    'costIndex_Medical_Supplies',
    'costIndex_Education',
    'costIndex_Electricity',
    'utilities_Fuels_And_Public_Services'
]
neighborhood_air_quality_attributes = [
    'ozone_Index',
    'lead_Index',
    'carbon_Monoxide_Index',
    'nitrogen_Dioxide_Index',
    'particulate_Matter_Index',
    'air_Pollution_Index'
]
neighborhood_climate_attributes = [
    'annual_Avg_Temp',
    'annual_Precip_In',
    'annual_Snowfall_In',
    'clear_Day_Mean',
    'rainy_Day_Mean'
]
neighborhood_natural_disaster_attributes = [
    'weather_Index',
    'hail_Index',
    'hurricane_Index',
    'tornado_Index',
    'wind_Index'
]
neighborhood_database_name = 'neighborhoods'

def get_neighborhood_df():
    neighborhood_geo_id_list = get_neighborhood_geo_id_list()
    data_dict = dict()
    attributes_lists = [
        (neighborhood_geography_attributes, 'geography'), 
        (neighborhood_demographics_attributes, 'demographics'), 
        (neighborhood_air_quality_attributes, 'airQuality'), 
        (neighborhood_climate_attributes, 'climate'), 
        (neighborhood_natural_disaster_attributes, 'naturalDisasters')
    ]
    for attributes, name in attributes_lists:
        for a in attributes:
            data_dict[a] = []
    for geo_id in neighborhood_geo_id_list:
        neighborhood_url = f"https://api.gateway.attomdata.com/v4/neighborhood/community?geoIdv4={geo_id}"
        response = requests.request("GET", neighborhood_url, headers=neighborhood_headers)
        json_data = response.json()
        print(f'{geo_id}: {response.status_code}')
        json_data = json_data['community']
        for attributes, name in attributes_lists:
            for a in attributes:
                data_dict[a].append(json_data[name][a])
    
    # print(data_dict)
    return pd.DataFrame(data_dict)
    
# The get_restaurants_and_write_to_file and get_restaurant_df methods scrape restaurant data from the Travel Advisor database.
# Just now I realized that I could just write the API response data to a temporary file.
# That way I don't have to keep calling the API every time I test something. 
# It is never a good time for the API to suddenly stop working.
def get_restaurants_and_write_to_file():
    offset = 0
    no_more_results = False
    all_restaurants = []
    while not no_more_results:
        url = "https://travel-advisor.p.rapidapi.com/restaurants/list"

        querystring = {"location_id":"42207", "offset": offset}

        headers = {
            "X-RapidAPI-Key": "####",
            "X-RapidAPI-Host": "travel-advisor.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers, params=querystring)
        json_dict = response.json()
        if len(json_dict['data']) > 0:
            all_restaurants += json_dict['data']
            offset += 30
        else:
            no_more_results = True
        time.sleep(1.0) # putting this here just in case because for my free plan I can only make 5 requests per second!

    with open('/Users/wlee2019/Downloads/temp.pkl', 'wb') as f:
        pickle.dump(all_restaurants, f)
        
# get_restaurants_and_write_to_file()
    
restaurant_attributes = [
    'name', 
    'address', 
    'description', 
    'cuisine', 
    'dietary_restrictions', 
    'num_reviews', 
    'rating', 
    'is_long_closed', 
    'price_level', 
    'website', 
    'phone', 
    'latitude', 
    'longitude', 
    # 'photo', 
    # 'hours', 
]
restaurant_database_name = 'restaurants'

def get_restaurant_df(attributes, name) -> tuple[pd.DataFrame, str]:
    with open('/Users/wlee2019/Downloads/temp.pkl', 'rb') as f:
        all_restaurants = pickle.load(f)
        # print(len(all_restaurants)) # at most 343 restaurants in Flint, Michigan.
        # print(all_restaurants[0].keys())
        # print(all_restaurants[0])
        data_dict = dict()
        for a in attributes:
            data_dict[a] = []
        for restaurant in all_restaurants:
            # print(restaurant)
            if 'ad_position' not in restaurant.keys():
                for a in attributes:
                    if a in restaurant.keys():
                        if a == 'cuisine' or a == 'dietary_restrictions':
                            list_str = ''
                            for ele in restaurant[a]:
                                list_str += f"{ele['name']},"
                            data_dict[a].append(list_str[:-1])
                        else:
                            try:
                                data_dict[a].append(restaurant[a])
                            except KeyError:
                                print(restaurant)
                                raise
                    else:
                        data_dict[a].append(None)

        df = pd.DataFrame(data_dict)
        df.insert(0, 'id', range(0, len(df)))
        print(df)
        return (df, name)


# dfs : list[tuple[pd.DataFrame, str]] = []
# dfs.append(get_provider_df(provider_attributes, provider_database_name))
# dfs.append((get_neighborhood_df(), neighborhood_database_name))
# dfs.append(get_restaurant_df(restaurant_attributes, restaurant_database_name))

schema="flint_aqua_advocates_db"
host="database-test1.cgyts2xn7st2.us-east-1.rds.amazonaws.com"
user="admin"
password="frickYoumyhomeboy!"
port=3306
con = f'mysql+pymysql://{user}:{password}@{host}:{port}/{schema}'

# This method writes a database "df" (with name "name") to the MySQL database.
def write(dfs):
    for df, name in dfs:
        print('writing to database...')
        df.to_sql(name, con=con, if_exists='replace', index=False)

# write(dfs)

# This method adds the latitude and longitude columns to a table.
# The latitude and longitude are found from searching on the Google Maps API.
def add_latitude_and_longitude_columns(df, table_name):
    # print(df)
    geolocation_url = 'https://maps.googleapis.com/maps/api/geocode/json?key=####'
    latitudes = []
    longitudes = []
    for index, row in df.iterrows():
        address = ''
        if table_name == provider_database_name:
            if row['address_line1'] != None:
                address += row['address_line1']
            if row['address_line2'] != None:
                address += f" {row['address_line2']}"
            address += ', MI'
            if row['zip_code'] != None:
                address += f", {row['zip_code']}"
            if address == ', MI':
                address = ''
        elif table_name == neighborhood_database_name:
            address += row['geographyName']
        else:
            address += f"{row['pwsName']}, MI"
        address = address.replace(' ', '+')
        # print(f'{geolocation_url}&address={address}')
        response = requests.request("GET", f'{geolocation_url}&address={address}')
        json_data = response.json()
        if json_data['status'] == 'OK':
            # check if the first result is within the bounds of Genesee County (assuming the first result is the best matching result)
            location = json_data['results'][0]['geometry']['location']
            latitude = location['lat']
            longitude = location['lng']
            if not within_genesee_county(latitude, longitude):
                latitude = None
                longitude = None
        # print(f'{latitude}, {longitude}')
        latitudes.append(latitude)
        longitudes.append(longitude)
    
    df['latitude'] = latitudes
    df['longitude'] = longitudes
    return df


# Approx. bounds of Genesee County (a small county that encircles Flint, MI)
def within_genesee_county(latitude, longitude):
    return latitude > 42.76999414802545 and latitude < 43.23535369887982 and longitude > -83.95899354100287 and longitude < -83.4315213546102

# dfs : list[tuple[pd.DataFrame, str]] = []
# for table_name in [provider_database_name, neighborhood_database_name, contaminant_database_name]:
#     dfs.append((add_latitude_and_longitude_columns(pd.read_sql(table_name, con), table_name), table_name))
    
# write(dfs)

import math
import re

NEARBY_THRESHOLD = 5

# Approx. distance from 2 lat-long locations
def calc_distance(latitude_1, longitude_1, latitude_2, longitude_2):
    return math.sqrt((((float(latitude_1) - float(latitude_2)) * 69) ** 2) + (((float(longitude_1) - float(longitude_2)) * 50) ** 2))

# Gets neighborhoods that are ~5 miles within each provider (latitude and longitude are used to approximate location and distance)
def provider_to_neighborhood(provider_df) :
    neighborhood_df = pd.read_sql(neighborhood_database_name, con)
    nearby_neighborhoods_dict = {
        'nearby_neighborhood_names': [],
        'nearby_neighborhood_ids': []
    }
    for index, row in provider_df.iterrows():
        nearby_neighborhood_names = ''
        nearby_neighborhood_ids = ''
        for i, r in neighborhood_df.iterrows():
            distance = calc_distance(row['latitude'], row['longitude'], r['latitude'], r['longitude'])
            if distance < NEARBY_THRESHOLD:
                nearby_neighborhood_names += re.split(',', r['geographyName'])[0] + ','
                nearby_neighborhood_ids += str(r['id']) + ','
        nearby_neighborhoods_dict['nearby_neighborhood_names'].append(nearby_neighborhood_names[:-1])
        nearby_neighborhoods_dict['nearby_neighborhood_ids'].append(nearby_neighborhood_ids[:-1])
    provider_df['nearby_neighborhood_names'] = nearby_neighborhoods_dict['nearby_neighborhood_names']
    provider_df['nearby_neighborhood_ids'] = nearby_neighborhoods_dict['nearby_neighborhood_ids']
    return provider_df

# Gets restaurants that are ~5 miles within each provider
def provider_to_restaurant(provider_df) :
    restaurant_df = pd.read_sql(restaurant_database_name, con)
    nearby_restaurants_dict = {
        'nearby_restaurant_names': [],
        'nearby_restaurant_ids': []
    }
    for index, row in provider_df.iterrows():
        nearby_restaurant_names = ''
        nearby_restaurant_ids = ''
        for i, r in restaurant_df.iterrows():
            distance = calc_distance(row['latitude'], row['longitude'], r['latitude'], r['longitude'])
            if distance < NEARBY_THRESHOLD:
                nearby_restaurant_names += re.split(',', r['name'])[0] + ','
                nearby_restaurant_ids += str(r['id']) + ','
        nearby_restaurants_dict['nearby_restaurant_names'].append(nearby_restaurant_names[:-1])
        nearby_restaurants_dict['nearby_restaurant_ids'].append(nearby_restaurant_ids[:-1])
    provider_df['nearby_restaurant_names'] = nearby_restaurants_dict['nearby_restaurant_names']
    provider_df['nearby_restaurant_ids'] = nearby_restaurants_dict['nearby_restaurant_ids']
    return provider_df

# Gets providers that are ~5 miles within each neighborhood
def neighborhood_to_provider(neighborhood_df):
    provider_df = pd.read_sql(provider_database_name, con)
    nearby_provider_dict = {
        'nearby_provider_names': [],
        'nearby_provider_ids': []
    }
    for index, row in neighborhood_df.iterrows():
        nearby_provider_names = ''
        nearby_provider_ids = ''
        for i, r in provider_df.iterrows():
            distance = calc_distance(row['latitude'], row['longitude'], r['latitude'], r['longitude'])
            if distance < NEARBY_THRESHOLD:
                nearby_provider_names += r['pwsName'] + ','
                nearby_provider_ids += r['pwsId'] + ','
        nearby_provider_dict['nearby_provider_names'].append(nearby_provider_names[:-1])
        nearby_provider_dict['nearby_provider_ids'].append(nearby_provider_ids[:-1])
    neighborhood_df['nearby_provider_names'] = nearby_provider_dict['nearby_provider_names']
    neighborhood_df['nearby_provider_ids'] = nearby_provider_dict['nearby_provider_ids']
    return neighborhood_df
    
# Gets restaurants that are ~5 miles within each neighborhood
def neighborhood_to_restaurant(neighborhood_df):
    restaurant_df = pd.read_sql(restaurant_database_name, con)
    nearby_restaurant_dict = {
        'nearby_restaurant_names': [],
        'nearby_restaurant_ids': []
    }
    for index, row in neighborhood_df.iterrows():
        nearby_restaurant_names = ''
        nearby_restaurant_ids = ''
        for i, r in restaurant_df.iterrows():
            distance = calc_distance(row['latitude'], row['longitude'], r['latitude'], r['longitude'])
            if distance < NEARBY_THRESHOLD:
                nearby_restaurant_names += r['name'] + ','
                nearby_restaurant_ids += str(r['id']) + ','
        nearby_restaurant_dict['nearby_restaurant_names'].append(nearby_restaurant_names[:-1])
        nearby_restaurant_dict['nearby_restaurant_ids'].append(nearby_restaurant_ids[:-1])
    neighborhood_df['nearby_restaurant_names'] = nearby_restaurant_dict['nearby_restaurant_names']
    neighborhood_df['nearby_restaurant_ids'] = nearby_restaurant_dict['nearby_restaurant_ids']
    return neighborhood_df

# Gets providers that are ~5 miles within each restaurant
def restaurant_to_provider(restaurant_df):
    provider_df = pd.read_sql(provider_database_name, con)
    nearby_providers_dict = {
        'nearby_provider_names': [],
        'nearby_provider_ids': []
    }
    for index, row in restaurant_df.iterrows():
        nearby_provider_names = ''
        nearby_provider_ids = ''
        for i, r in provider_df.iterrows():
            distance = calc_distance(row['latitude'], row['longitude'], r['latitude'], r['longitude'])
            if distance < NEARBY_THRESHOLD:
                nearby_provider_names += r['pwsName'] + ','
                nearby_provider_ids += r['pwsId'] + ','
        nearby_providers_dict['nearby_provider_names'].append(nearby_provider_names[:-1])
        nearby_providers_dict['nearby_provider_ids'].append(nearby_provider_ids[:-1])
    restaurant_df['nearby_provider_names'] = nearby_providers_dict['nearby_provider_names']
    restaurant_df['nearby_provider_ids'] = nearby_providers_dict['nearby_provider_ids']
    return restaurant_df

# Gets neighborhoods that are ~5 miles within each restaurant
def restaurant_to_neighborhood(restaurant_df):
    neighborhood_df = pd.read_sql(neighborhood_database_name, con)
    nearby_neighborhoods_dict = {
        'nearby_neighborhood_names': [],
        'nearby_neighborhood_ids': []
    }
    for index, row in restaurant_df.iterrows():
        nearby_neighborhood_names = ''
        nearby_neighborhood_ids = ''
        for i, r in neighborhood_df.iterrows():
            distance = calc_distance(row['latitude'], row['longitude'], r['latitude'], r['longitude'])
            if distance < NEARBY_THRESHOLD:
                nearby_neighborhood_names += re.split(',', r['geographyName'])[0] + ','
                nearby_neighborhood_ids += str(r['id']) + ','
        nearby_neighborhoods_dict['nearby_neighborhood_names'].append(nearby_neighborhood_names[:-1])
        nearby_neighborhoods_dict['nearby_neighborhood_ids'].append(nearby_neighborhood_ids[:-1])
    restaurant_df['nearby_neighborhood_names'] = nearby_neighborhoods_dict['nearby_neighborhood_names']
    restaurant_df['nearby_neighborhood_ids'] = nearby_neighborhoods_dict['nearby_neighborhood_ids']
    return restaurant_df

    
# new_provider_df = pd.read_sql(provider_database_name, con)
# # new_provider_df = provider_to_neighborhood(new_provider_df)
# new_provider_df = provider_to_restaurant(new_provider_df)
# new_neighborhood_df = pd.read_sql(neighborhood_database_name, con)
# # new_neighborhood_df = neighborhood_to_provider(new_neighborhood_df)
# new_neighborhood_df = neighborhood_to_restaurant(new_neighborhood_df)
# new_restaurant_df = pd.read_sql(restaurant_database_name, con)
# new_restaurant_df = restaurant_to_provider(new_restaurant_df)
# new_restaurant_df = restaurant_to_neighborhood(new_restaurant_df)

# dfs : list[tuple[pd.DataFrame, str]] = []
# dfs.append((new_provider_df, provider_database_name))
# dfs.append((new_neighborhood_df, neighborhood_database_name))
# dfs.append((new_restaurant_df, restaurant_database_name))
# write(dfs)

import json
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from io import BytesIO

# This method adds an image link attribute to each instance. Now each instance is associated with a unique image.
# Used Google Search API
def add_image_link_to_db(df, name_attribute) :
    print('adding image link column to model')
    url = 'https://www.googleapis.com/customsearch/v1'
    image_links = []
    for index, row in df.iterrows():
        # print(f'{index}: {row[name_attribute]}')
        params = {
            'cx': '20e1ef67026214b80',
            'q': row[name_attribute] + ' Flint, Michigan',
            'searchType': 'image',
            'key': 'AIzaSyCfRsCepylJOiAxaizfDhXtDcKBh_zeMAM',
            'safe': 'active'
        }
        response = requests.get(url, params=params)
        # print(json.dumps(response.json(), indent=2))
        json_data = response.json()
        image_link = ''
        for i in range(min(10, int(json_data['searchInformation']['totalResults']))):
            image_link = json_data['items'][i]['link']
            # print(image_link)
            valid_image = True
            try:
                # https://stackoverflow.com/a/62599037
                # headers is set so that the server hosting the image would be less likely to deny the request
                req = requests.get(image_link, stream=True, headers={"User-Agent": "Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36"}, timeout=3)
                # img = Image.open(BytesIO(req.content))
                # plt.imshow(np.asarray(img))
                # plt.draw()
                # plt.pause(1)
                # plt.close()
                # img.close()
                if req.status_code != 200:
                    valid_image = False
            except Exception:
                valid_image = False
            if valid_image:
                break
        image_links.append(image_link)
    df['image_link'] = image_links
    return df

# new_provider_df = pd.read_sql(provider_database_name, con)
# new_provider_df = add_image_link_to_db(new_provider_df, 'pwsName')
# new_neighborhood_df = pd.read_sql(neighborhood_database_name, con)
# new_neighborhood_df = add_image_link_to_db(new_neighborhood_df, 'geographyName')
# new_restaurant_df = pd.read_sql(restaurant_database_name, con)
# new_restaurant_df = add_image_link_to_db(new_restaurant_df, 'name')

# dfs : list[tuple[pd.DataFrame, str]] = []
# dfs.append((new_provider_df, provider_database_name + '_with_image_links'))
# dfs.append((new_neighborhood_df, neighborhood_database_name + '_with_image_links'))
# dfs.append((new_restaurant_df, restaurant_database_name + '_with_image_links'))
# write(dfs)
