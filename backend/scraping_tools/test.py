import json
import urllib
import requests

where = urllib.parse.quote_plus("""
{
    "Primary_city": {
        "$exists": true
    }
}
""")
url = 'https://parseapi.back4app.com/classes/Uszipcode_US_Zip_Code?count=1&limit=0&where=%s' % where
headers = {
    'X-Parse-Application-Id': 'xASxLAwMKlxzEkGyl2gEIRtFxOb3p9w46QlmJo5z', # This is your app's application id
    'X-Parse-REST-API-Key': 'Six5Z3FWuBGovlliefJj4yzmpG7fT6G3Wabg5pus' # This is your app's REST API key
}
data = json.loads(requests.get(url, headers=headers).content.decode('utf-8')) # Here you have the data that you need
print(json.dumps(data, indent=2))