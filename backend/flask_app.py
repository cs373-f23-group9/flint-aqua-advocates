from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy 
from sqlalchemy import Column, Integer, String, Sequence, Float, BigInteger, or_, and_
import logging
from dotenv import load_dotenv
import os

load_dotenv()

# Configuration
RESULTS_PER_PAGE = 25

schema = os.getenv('DB_SCHEMA')
host = os.getenv('DB_HOST')
user = os.getenv('DB_USER')
password = os.getenv('DB_PASSWORD')
port = 3306

db = SQLAlchemy()

#sets ups app destination and initalizes and returns app
def create_app(config_name='default'):
    app = Flask(__name__)

    if config_name == 'testing':
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        app.config['TESTING'] = True
    else:
        app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://{user}:{password}@{host}:{port}/{schema}'

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # Suppress warning
    
    init_app(app)
    return app


# Model Definition
class WaterProviders(db.Model):
    __tablename__ = 'water_providers'  # Assuming the table name

    pwsId = db.Column(db.Text, primary_key=True)  # Updated to Text
    pwsName = db.Column(db.Text)  # Updated to Text
    population_served_count = db.Column(db.BigInteger)  # Remains unchanged
    address_line1 = db.Column(db.Text)  # Updated to Text
    address_line2 = db.Column(db.Text)  # Updated to Text
    zip_code = db.Column(db.Text)  # Updated to Text
    pws_deactivation_date = db.Column(db.Text)  # Updated to Text
    season_begin_date = db.Column(db.Text)  # Updated to Text
    season_end_date = db.Column(db.Text)  # Updated to Text
    email_addr = db.Column(db.Text)  # Updated to Text
    phone_number = db.Column(db.Text)  # Updated to Text
    latitude = db.Column(db.Float(precision='double'))  # Updated to Double Precision Float
    longitude = db.Column(db.Float(precision='double'))  # Updated to Double Precision Float
    nearby_neighborhood_names = db.Column(db.Text)  # Updated to Text
    nearby_neighborhood_ids = db.Column(db.Text)  # Updated to Text
    nearby_restaurant_names = db.Column(db.Text)  # New column
    nearby_restaurant_ids = db.Column(db.Text)  # New column
    image_link = db.Column(db.Text)

class Restaurants(db.Model):
    __tablename__ = 'restaurants'

    id = db.Column(db.BigInteger, primary_key=True) #starts with 0
    name = db.Column(db.Text)
    address = db.Column(db.Text)
    description = db.Column(db.Text)
    cuisine = db.Column(db.Text)
    dietary_restrictions = db.Column(db.Text)
    num_reviews = db.Column(db.Text)
    rating = db.Column(db.Text)
    is_long_closed = db.Column(db.BigInteger)
    price_level = db.Column(db.Text)
    website = db.Column(db.Text)
    phone = db.Column(db.Text)
    latitude = db.Column(db.Text)
    longitude = db.Column(db.Text)
    nearby_provider_names = db.Column(db.Text)
    nearby_provider_ids = db.Column(db.Text)
    nearby_neighborhood_names = db.Column(db.Text)
    nearby_neighborhood_ids = db.Column(db.Text)
    image_link = db.Column(db.Text)
    
class Neighborhoods(db.Model):
    __tablename__ = 'neighborhoods'
    
    id = db.Column(db.Integer, primary_key=True)  # Primary key column, starts with 1
    
    geographyName = db.Column(db.String, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    population = db.Column(db.BigInteger, nullable=True)
    population_Urban_Pct = db.Column(db.Float, nullable=True)
    population_Rural_Pct = db.Column(db.Float, nullable=True)
    population_White_Pct = db.Column(db.Float, nullable=True)
    population_Black_Pct = db.Column(db.Float, nullable=True)
    population_Asian_Pct = db.Column(db.Float, nullable=True)
    population_American_Indian_Or_Alaskan_Native_Pct = db.Column(db.Float, nullable=True)
    housing_Units = db.Column(db.BigInteger, nullable=True)
    housing_Units_Vacant = db.Column(db.BigInteger, nullable=True)
    housing_Median_Rent = db.Column(db.BigInteger, nullable=True)
    avg_Household_Income = db.Column(db.BigInteger, nullable=True)
    costIndex_Annual_Expenditures = db.Column(db.Float, nullable=True)
    costIndex_Food = db.Column(db.Float, nullable=True)
    costIndex_Non_Alcoholic_Beverages = db.Column(db.Float, nullable=True)
    costIndex_Local_Restaurants = db.Column(db.Float, nullable=True)
    costIndex_Alcoholic_Beverages = db.Column(db.Float, nullable=True)
    costIndex_Housing = db.Column(db.Float, nullable=True)
    costIndex_Gasoline_And_Motor_Oil = db.Column(db.Float, nullable=True)
    costIndex_Public_Transportation = db.Column(db.Float, nullable=True)
    costIndex_Healthcare = db.Column(db.Float, nullable=True)
    costIndex_Health_Insurance = db.Column(db.Float, nullable=True)
    costIndex_Medical_Supplies = db.Column(db.Float, nullable=True)
    costIndex_Education = db.Column(db.Float, nullable=True)
    costIndex_Electricity = db.Column(db.Float, nullable=True)
    utilities_Fuels_And_Public_Services = db.Column(db.Float, nullable=True)
    ozone_Index = db.Column(db.BigInteger, nullable=True)
    lead_Index = db.Column(db.BigInteger, nullable=True)
    carbon_Monoxide_Index = db.Column(db.BigInteger, nullable=True)
    nitrogen_Dioxide_Index = db.Column(db.BigInteger, nullable=True)
    particulate_Matter_Index = db.Column(db.BigInteger, nullable=True)
    air_Pollution_Index = db.Column(db.BigInteger, nullable=True)
    annual_Avg_Temp = db.Column(db.Float, nullable=True)
    annual_Precip_In = db.Column(db.Float, nullable=True)
    annual_Snowfall_In = db.Column(db.BigInteger, nullable=True)
    clear_Day_Mean = db.Column(db.BigInteger, nullable=True)
    rainy_Day_Mean = db.Column(db.BigInteger, nullable=True)
    weather_Index = db.Column(db.BigInteger, nullable=True)
    hail_Index = db.Column(db.BigInteger, nullable=True)
    hurricane_Index = db.Column(db.BigInteger, nullable=True)
    tornado_Index = db.Column(db.BigInteger, nullable=True)
    wind_Index = db.Column(db.BigInteger, nullable=True)
    nearby_provider_names = db.Column(db.Text)
    nearby_provider_ids = db.Column(db.Text)
    nearby_restaurant_names = db.Column(db.Text)  
    nearby_restaurant_ids = db.Column(db.Text)
    image_link = db.Column(db.Text)  

 #filterable attributes

provider_filters = {
    'population_served_count': lambda model, arg: and_(model.population_served_count >= int(arg.split(',')[0]), model.population_served_count <= int(arg.split(',')[1])),
    'address': lambda model, arg: or_(model.address_line1.ilike(f"%{arg}%"), model.address_line2.ilike(f"%{arg}%")),
    'zip_code': lambda model, arg: model.zip_code.ilike(f"%{arg}%"),
    'phone_number': lambda model, arg : model.phone_number.ilike(f"%{arg}%"),
    'email': lambda model, arg : model.email_addr.ilike(f"%{arg}%"),
}

restaurant_filters = {
    'address': lambda model, arg: model.address.ilike(f"%{arg}%"),
    'cuisine': lambda model, arg: model.cuisine.ilike(f"%{arg}%"),
    'dietary_restrictions': lambda model, arg: model.dietary_restrictions.ilike(f"%{arg}%"),
    'rating': lambda model, arg: and_(model.rating >= int(arg.split(',')[0]), model.rating <= int(arg.split(',')[1])),
    'phone': lambda model, arg : model.phone.ilike(f"%{arg}%")
}

neighborhood_filters = {
    'population': lambda model, arg: and_(model.population >= int(arg.split(',')[0]), model.population <= int(arg.split(',')[1])),
    'avg_Household_Income': lambda model, arg: and_(model.avg_Household_Income >= int(arg.split(',')[0]), model.avg_Household_Income <= int(arg.split(',')[1])),
    'housing_Median_Rent': lambda model, arg: and_(model.housing_Median_Rent >= int(arg.split(',')[0]), model.housing_Median_Rent <= int(arg.split(',')[1])),
    'housing_Units': lambda model, arg: and_(model.housing_Units >= int(arg.split(',')[0]), model.housing_Units <= int(arg.split(',')[1])),
    'housing_Units_Vacant': lambda model, arg: and_(model.housing_Units_Vacant >= int(arg.split(',')[0]), model.housing_Units_Vacant <= int(arg.split(',')[1])),
}

def apply_search(query, model, search_terms):
    results = []
    for term in search_terms:
        for column in model.__table__.columns:
            results.append(column.ilike(f"%{term}%"))
    return query.filter(or_(*results))

def apply_filters(query, model, filters):
    for filter_name, filter_condition in filters.items():
        arg = request.args.get(filter_name, type=str)
        if arg is not None:
            query = query.filter(filter_condition(model, arg))
    return query

def apply_sort(query, model):
    sort_attribute = request.args.get('sort_attribute')
    sort_order = request.args.get('sort')
    if sort_attribute and sort_order:
        column = getattr(model, sort_attribute, None)
        if column:
            if sort_order.lower() == 'asc':
                query = query.order_by(column.asc())
            elif sort_order.lower() == 'desc':
                query = query.order_by(column.desc())
    return query

def process_query(model, filters, process_result_func):
    query = model.query

    search = request.args.get("search", type=str)
    if search:
        search_terms = search.split(",")
        query = apply_search(query, model, search_terms)

    query = apply_filters(query, model, filters)
    query = apply_sort(query, model)

    try:
        total_count = query.count()
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', RESULTS_PER_PAGE, type=int)
        results = query.paginate(page=page, per_page=per_page, error_out=False).items

        processed_results = [process_result_func(result) for result in results]

        return jsonify({
            'total_count': total_count,
            'response': processed_results,
        }), 200
    except Exception as e:
        return str(e), 500

#initalizes the app and defines endpoints
def init_app(app): 
    db.init_app(app)
    CORS(app)
        
    #providers
    @app.route('/providers', methods=['GET'])
    def fetch_water_providers():
        #controls what is returned to make front end deal with less entries
        def process_provider_result(provider):
            """
            Process a single water provider record and return desired fields.
            """
            return {
                "name": provider.pwsName,
                "id": provider.pwsId,
                "population_served_count": provider.population_served_count,
                "address": f"{provider.address_line1} {provider.address_line2}".strip(),
                "phone_number": provider.phone_number,
                "email": provider.email_addr,
                "zip_code": provider.zip_code,
                "longitude": provider.longitude,
                "latitude": provider.latitude,
                #"related_neighborhood_names": provider.nearby_neighborhood_names,
                "related_neighborhood_ids": provider.nearby_neighborhood_ids,
                #"related_restaurant_names": provider.nearby_restaurant_names,
                "related_restaurant_ids": provider.nearby_restaurant_ids,
                "image_link": provider.image_link
            }
            
        # Check if pwsId is in the query parameters
        pwsId = request.args.get('pwsId')
        # Fetch a specific water provider by pwsId
        if pwsId:
            try:
                result = db.session.get(WaterProviders, pwsId)
                if result is None:
                    return jsonify({'error': 'Entry not found'}), 404

                return jsonify(process_provider_result(result)), 200
            except Exception as e:
                return str(e), 500
        else:
            return process_query(WaterProviders, provider_filters, process_provider_result)

    @app.route('/providers/filters', methods=['GET'])
    def fetch_water_providers_filters():
        addresses = db.session.query(WaterProviders.address_line1, WaterProviders.address_line2).distinct().all()
        zip_codes = db.session.query(WaterProviders.zip_code).distinct().all()
        phone_numbers = db.session.query(WaterProviders.phone_number).distinct().all()
        emails = db.session.query(WaterProviders.email_addr).distinct().all()
        return jsonify([
            {
                "name": "Population Served",
                "subOptions": ["0-20", "20-40", "40-100", "100+"],
                "queryName": "population_served_count",
                "type": "range"
            },
            {
                "name": "Address",
                "subOptions": [f"{address[0]}, {address[1]}" if address[1] else address[0] for address in addresses],
                "queryName": "address",
                "type": "match"
            },
            {
                "name": "Zip Code",
                "subOptions": [zip_code[0] for zip_code in zip_codes],
                "queryName": "zip_code",
                "type": "match"
            },
            {
                "name": "Phone Number",
                "subOptions": [phone_number[0] for phone_number in phone_numbers],
                "queryName": "phone_number",
                "type": "match"
            },
            {
                "name": "Email",
                "subOptions": [email[0] for email in emails],
                "queryName": "email",
                "type": "match"
            }
        ]), 200
                
             
    @app.route('/restaurants', methods=['GET'])
    def fetch_restaurants():
        def process_restaurants_result(restaurant):
            """
            Process a single restaurant record and return all fields.
            """
            return {
                "id": restaurant.id,
                "name": restaurant.name,
                "address": restaurant.address,
                "description": restaurant.description,
                "cuisine": restaurant.cuisine,
                "dietary_restrictions": restaurant.dietary_restrictions,
                "num_reviews": restaurant.num_reviews,
                "rating": restaurant.rating,
                "is_long_closed": restaurant.is_long_closed,
                "price_level": restaurant.price_level,
                "website": restaurant.website,
                "phone": restaurant.phone,
                "latitude": restaurant.latitude,
                "longitude": restaurant.longitude,
                #"related_provider_names": restaurant.nearby_provider_names,
                "related_provider_ids": restaurant.nearby_provider_ids,
                #"related_neighborhood_names": restaurant.nearby_neighborhood_names,
                "related_neighborhood_ids": restaurant.nearby_neighborhood_ids,
                "image_link": restaurant.image_link
            }

        # Check if id is in the query parameters
        id = request.args.get('id')
        
        if id:
            # Fetch a specific restaurant by id
            try:
                result = db.session.get(Restaurants, id)
                if result is None:
                    return jsonify({'error': 'Entry not found'}), 404

                return jsonify(process_restaurants_result(result)), 200
            except Exception as e:
                return str(e), 500

        else:
            # Fetch all restaurants
            return process_query(Restaurants, restaurant_filters, process_restaurants_result)
        
    @app.route('/restaurants/filters', methods=['GET'])
    def fetch_restaurants_filters():
        # used for options consisting of comma separated lists
        def format_query_options(options):
            # format cuisine list
            options_set = set()
            for opt in options:
                for opt_type in opt[0].split(','):
                    if len(opt_type) > 0:
                        options_set.add(opt_type)
            formatted = list(options_set)
            formatted.sort()
            return formatted

        addresses = db.session.query(Restaurants.address).distinct().all()
        cuisines = db.session.query(Restaurants.cuisine).distinct().all()
        dietary_restrictions = db.session.query(Restaurants.dietary_restrictions).distinct().all()
        phones = db.session.query(Restaurants.phone).distinct().all()
        
        return jsonify([
            {
                "name": "Address",
                "subOptions": [address[0] for address in addresses],
                "queryName": "address",
                "type": "match"
            },
            {
                "name": "Cuisine",
                "subOptions": format_query_options(cuisines),
                "queryName": "cuisine",
                "type": "match"
            },
            {
                "name": "Dietary Restrictions",
                "subOptions": format_query_options(dietary_restrictions),
                "queryName": "dietary_restrictions",
                "type": "match"
            },
            {
                "name": "Rating",
                "subOptions": ["0-1", "1-2", "2-3", "3-4", "4-5"],
                "queryName": "rating",
                "type": "range"
            },
            {
                "name": "Phone",
                "subOptions": [phone[0] for phone in phones],
                "queryName": "phone",
                "type": "match"
            }
        ]), 200

    #Neighborhoods:

    @app.route('/neighborhoods', methods=['GET'])
    def fetch_neighborhoods():
        def process_neighborhood_result(neighborhood):
            """
            Process a single neighborhood record and return the desired fields.
            """
            return {
                "id": neighborhood.id,
                "name": neighborhood.geographyName,
                "population": neighborhood.population,
                "avg_Household_Income": neighborhood.avg_Household_Income,
                "housing_Median_Rent": neighborhood.housing_Median_Rent,
                "housing_Units": neighborhood.housing_Units,
                "housing_Units_Vacant": neighborhood.housing_Units_Vacant,
                "latitude": neighborhood.latitude,
                "longitude": neighborhood.longitude,
                #"related_provider_names": neighborhood.nearby_provider_names,
                "related_provider_ids": neighborhood.nearby_provider_ids,
                #"related_restaurant_names": neighborhood.nearby_restaurant_names,
                "related_restaurant_ids": neighborhood.nearby_restaurant_ids,
                "image_link": neighborhood.image_link
            }

        # Check if id is in the query parameters
        id = request.args.get('id', type=int)
        
        if id:
            # Fetch a specific neighborhood by id
            try:
                result = db.session.get(Neighborhoods, id)
                if result is None:
                    return jsonify({'error': 'Neighborhood not found'}), 404

                return jsonify(process_neighborhood_result(result)), 200
            except Exception as e:
                return str(e), 500

        else:
            return process_query(Neighborhoods, neighborhood_filters, process_neighborhood_result)
    
    @app.route('/neighborhoods/filters', methods=['GET'])
    def fetch_neighborhood_filters():
        return jsonify([
            {
                "name": "Population",
                "subOptions": ["0-20", "20-40", "40-100", "100+"],
                "queryName": "population",
                "type": "range"
            },
            {
                "name": "Average Household Income",
                "subOptions": ["$0-20000", "$20000-40000", "$40000+"],
                "queryName": "avg_Household_Income",
                "type": "range"
            },
            {
                "name": "Median Rent",
                "subOptions": ["$0-300", "$300-500", "$500+"],
                "queryName": "housing_Median_Rent",
                "type": "range"
            },
            {
                "name": "Total Housing Units",
                "subOptions": ["0-500", "500-1000", "1000+"],
                "queryName": "housing_Units",
                "type": "range"
            },
            {
                "name": "Available Housing Units",
                "subOptions": ["0-200", "200-500", "500+"],
                "queryName": "housing_Units_Vacant",
                "type": "range"
            }
        ]), 200

    @app.route('/visualization-data', methods=['GET'])
    def fetch_vis_data():
        return jsonify({
            "providersByPopulation": [
                { "name": "0-20", "count": 6 },
                { "name": "20-40", "count": 23 },
                { "name": "40-100", "count": 19 },
                { "name": "100+", "count": 20 },
            ],
            "neighborhoodsByRent": [
                { "name": "0-400", "count": 3 },
                { "name": "400-500", "count": 3 },
                { "name": "500-600", "count": 9 },
                { "name": "600-700", "count": 4 },
                { "name": "700+", "count": 2 },
            ],
            "restaurantsByRating": [
                {"name": "0-1", "count": 2},
                {"name": "1-2", "count": 10},
                {"name": "2-3", "count": 45},
                {"name": "3-4", "count": 170},
                {"name": "4-5", "count": 156},
            ]
        })
    
    @app.route('/provider-visualization-data', methods=['GET'])
    def fetch_provider_vis_data():
        return jsonify({
            "scholarshipsByAid": [
                { "name": "<= 1000", "count": 47 },
                { "name": "1001-2000", "count": 20 },
                { "name": "2001-3000", "count": 19 },
                { "name": "3001-4000", "count": 2 },
                { "name": "4001-5000", "count": 4 },
                { "name": "5001-10000", "count": 4 },
                { "name": ">= 10000", "count": 4 }
            ],
            "cityScholarships": [
                {
                "name": "Alpine, TX",

                "lat": 30.3561488,

                "lon": -103.6696958,
                "numScholarships": 3
                },

                {
                "name": "Addison, TX",

                "lat": 32.96179,

                "lon": -96.82916850000001,
                "numScholarships": 6
                },

                {
                "name": "Alvin, TX",

                "lat": 29.4238472,

                "lon": -95.24410089999999,
                "numScholarships": 3
                },

                {
                "name": "Athens, TX",

                "lat": 32.2048735,

                "lon": -95.8555207,
                "numScholarships": 3
                },

                {
                "name": "Borger, TX",

                "lat": 35.6678203,

                "lon": -101.3973876,
                "numScholarships": 3
                },

                {
                "name": "Bastrop, TX",

                "lat": 30.1104947,

                "lon": -97.3152701,
                "numScholarships": 3
                },

                {
                "name": "Beaumont, TX",

                "lat": 30.080174,

                "lon": -94.1265562,
                "numScholarships": 12
                },

                {
                "name": "Beeville, TX",

                "lat": 28.4008319,

                "lon": -97.7483312,
                "numScholarships": 3
                },

                {
                "name": "Big Spring, TX",

                "lat": 32.2503979,

                "lon": -101.4787355,
                "numScholarships": 6
                },

                {
                "name": "Baytown, TX",

                "lat": 29.7355047,

                "lon": -94.97742740000001,
                "numScholarships": 3
                },

                {
                "name": "Belton, TX",

                "lat": 31.0560132,

                "lon": -97.46445299999999,
                "numScholarships": 3
                },

                {
                "name": "Bedford, TX",

                "lat": 32.844017,

                "lon": -97.1430671,
                "numScholarships": 6
                },

                {
                "name": "Benbrook, TX",

                "lat": 32.673188,

                "lon": -97.4605759,
                "numScholarships": 3
                },

                {
                "name": "Brownwood, TX",

                "lat": 31.7093197,

                "lon": -98.9911611,
                "numScholarships": 3
                },

                {
                "name": "Cedar Hill, TX",

                "lat": 32.5884689,

                "lon": -96.9561152,
                "numScholarships": 3
                },

                {
                "name": "Brenham, TX",

                "lat": 30.1668828,

                "lon": -96.39774419999999,
                "numScholarships": 9
                },

                {
                "name": "Carthage, TX",

                "lat": 32.1573841,

                "lon": -94.3374199,
                "numScholarships": 3
                },

                {
                "name": "Canyon, TX",

                "lat": 34.9803342,

                "lon": -101.9188024,
                "numScholarships": 3
                },

                {
                "name": "Cisco, TX",

                "lat": 32.38818610000001,

                "lon": -98.97923360000001,
                "numScholarships": 3
                },

                {
                "name": "Clute, TX",

                "lat": 29.0246906,

                "lon": -95.3988291,
                "numScholarships": 3
                },

                {
                "name": "Clarendon, TX",

                "lat": 34.9378289,

                "lon": -100.8881993,
                "numScholarships": 3
                },

                {
                "name": "Commerce, TX",

                "lat": 33.2472808,

                "lon": -95.9114704,
                "numScholarships": 3
                },

                {
                "name": "College Station, TX",

                "lat": 30.627977,

                "lon": -96.3344068,
                "numScholarships": 3
                },

                {
                "name": "Conroe, TX",

                "lat": 30.3118769,

                "lon": -95.45605119999999,
                "numScholarships": 3
                },

                {
                "name": "Denton, TX",

                "lat": 33.2148412,

                "lon": -97.13306829999999,
                "numScholarships": 9
                },

                {
                "name": "Denison, TX",

                "lat": 33.7556593,

                "lon": -96.53665799999999,
                "numScholarships": 3
                },

                {
                "name": "Corsicana, TX",

                "lat": 32.0954304,

                "lon": -96.46887269999999,
                "numScholarships": 3
                },

                {
                "name": "Del Rio, TX",

                "lat": 29.3708857,

                "lon": -100.8958674,
                "numScholarships": 3
                },

                {
                "name": "El Paso, TX",

                "lat": 31.7618778,

                "lon": -106.4850217,
                "numScholarships": 37
                },

                {
                "name": "Edinburg, TX",

                "lat": 26.3017374,

                "lon": -98.1633432,
                "numScholarships": 3
                },

                {
                "name": "Farmers Branch, TX",

                "lat": 32.9265137,

                "lon": -96.89611509999999,
                "numScholarships": 3
                },

                {
                "name": "Eagle Pass, TX",

                "lat": 28.7091433,

                "lon": -100.4995214,
                "numScholarships": 3
                },

                {
                "name": "Fort Worth, TX",

                "lat": 32.7554883,

                "lon": -97.3307658,
                "numScholarships": 33
                },

                {
                "name": "Frisco, TX",

                "lat": 33.1506744,

                "lon": -96.82361159999999,
                "numScholarships": 3
                },

                {
                "name": "Gainesville, TX",

                "lat": 33.62594139999999,

                "lon": -97.1333453,
                "numScholarships": 3
                },

                {
                "name": "Garland, TX",

                "lat": 32.912624,

                "lon": -96.63888329999999,
                "numScholarships": 6
                },

                {
                "name": "Georgetown, TX",

                "lat": 30.6332618,

                "lon": -97.6779842,
                "numScholarships": 6
                },

                {
                "name": "Galveston, TX",

                "lat": 29.3013479,

                "lon": -94.7976958,
                "numScholarships": 6
                },

                {
                "name": "Grand Prairie, TX",

                "lat": 32.7459645,

                "lon": -96.99778459999999,
                "numScholarships": 18
                },

                {
                "name": "Harlingen, TX",

                "lat": 26.1906306,

                "lon": -97.69610259999999,
                "numScholarships": 6
                },

                {
                "name": "Greenville, TX",

                "lat": 33.1398034,

                "lon": -96.1072021,
                "numScholarships": 3
                },

                {
                "name": "Haltom city, TX",

                "lat": 32.7995738,

                "lon": -97.26918169999999,
                "numScholarships": 3
                },

                {
                "name": "Hawkins, TX",

                "lat": 32.58847350000001,

                "lon": -95.20411349999999,
                "numScholarships": 3
                },

                {
                "name": "Houston, TX",

                "lat": 29.7604267,

                "lon": -95.3698028,
                "numScholarships": 198
                },

                {
                "name": "Irving, TX",

                "lat": 32.8140177,

                "lon": -96.9488945,
                "numScholarships": 21
                },

                {
                "name": "Hillsboro, TX",

                "lat": 32.0109886,

                "lon": -97.13000609999999,
                "numScholarships": 3
                },

                {
                "name": "Huntsville, TX",

                "lat": 30.7235263,

                "lon": -95.55077709999999,
                "numScholarships": 6
                },

                {
                "name": "Hurst, TX",

                "lat": 32.8234621,

                "lon": -97.1705678,
                "numScholarships": 3
                },

                {
                "name": "Jacksonville, TX",

                "lat": 31.963778,

                "lon": -95.27050419999999,
                "numScholarships": 6
                },

                {
                "name": "Jasper, TX",

                "lat": 30.920823,

                "lon": -93.99688119999999,
                "numScholarships": 3
                },

                {
                "name": "Killeen, TX",

                "lat": 31.1171194,

                "lon": -97.72779589999999,
                "numScholarships": 9
                },

                {
                "name": "Laredo, TX",

                "lat": 27.5035613,

                "lon": -99.5075519,
                "numScholarships": 15
                },

                {
                "name": "La Joya, TX",

                "lat": 26.2470165,

                "lon": -98.4814092,
                "numScholarships": 3
                },

                {
                "name": "Kingsville, TX",

                "lat": 27.5158689,

                "lon": -97.85610899999999,
                "numScholarships": 3
                },

                {
                "name": "Lake Jackson, TX",

                "lat": 29.0338575,

                "lon": -95.4343859,
                "numScholarships": 3
                },

                {
                "name": "Keene, TX",

                "lat": 32.3968092,

                "lon": -97.3239042,
                "numScholarships": 3
                },

                {
                "name": "Kerrville, TX",

                "lat": 30.0474332,

                "lon": -99.1403189,
                "numScholarships": 6
                },

                {
                "name": "Kilgore, TX",

                "lat": 32.3862619,

                "lon": -94.87577089999999,
                "numScholarships": 3
                },

                {
                "name": "Lewisville, TX",

                "lat": 33.0463292,

                "lon": -96.9941903,
                "numScholarships": 3
                },

                {
                "name": "Longview, TX",

                "lat": 32.5007037,

                "lon": -94.74048909999999,
                "numScholarships": 6
                },

                {
                "name": "Lubbock, TX",

                "lat": 33.5845617,

                "lon": -101.8456417,
                "numScholarships": 18
                },

                {
                "name": "Levelland, TX",

                "lat": 33.5873164,

                "lon": -102.37796,
                "numScholarships": 3
                },

                {
                "name": "Lufkin, TX",

                "lat": 31.3345562,

                "lon": -94.7321127,
                "numScholarships": 6
                },

                {
                "name": "Mission, TX",

                "lat": 26.2159066,

                "lon": -98.32529319999999,
                "numScholarships": 3
                },

                {
                "name": "McAllen, TX",

                "lat": 26.2034071,

                "lon": -98.23001239999999,
                "numScholarships": 24
                },

                {
                "name": "McKinney, TX",

                "lat": 33.1983388,

                "lon": -96.6389342,
                "numScholarships": 3
                },

                {
                "name": "Mesquite, TX",

                "lat": 32.76679550000001,

                "lon": -96.5991593,
                "numScholarships": 6
                },

                {
                "name": "Midland, TX",

                "lat": 31.9973456,

                "lon": -102.0779146,
                "numScholarships": 3
                },

                {
                "name": "Marshall, TX",

                "lat": 32.5448714,

                "lon": -94.36741839999999,
                "numScholarships": 6
                },

                {
                "name": "New Braunfels, TX",

                "lat": 29.702566,

                "lon": -98.12406349999999,
                "numScholarships": 6
                },

                {
                "name": "Mount Pleasant, TX",

                "lat": 33.1567863,

                "lon": -94.96826899999999,
                "numScholarships": 3
                },

                {
                "name": "Nacogdoches, TX",

                "lat": 31.6039455,

                "lon": -94.65600400000001,
                "numScholarships": 3
                },

                {
                "name": "Odessa, TX",

                "lat": 31.8456816,

                "lon": -102.3676431,
                "numScholarships": 6
                },

                {
                "name": "Orange, TX",

                "lat": 30.0929879,

                "lon": -93.7365549,
                "numScholarships": 3
                },

                {
                "name": "Pharr, TX",

                "lat": 26.1947962,

                "lon": -98.1836216,
                "numScholarships": 6
                },

                {
                "name": "Portland, TX",

                "lat": 27.8772463,

                "lon": -97.3238805,
                "numScholarships": 3
                },

                {
                "name": "Paris, TX",

                "lat": 33.6609389,

                "lon": -95.55551299999999,
                "numScholarships": 3
                },

                {
                "name": "Prairie View, TX",

                "lat": 30.0932737,

                "lon": -95.9877339,
                "numScholarships": 3
                },

                {
                "name": "Pasadena, TX",

                "lat": 29.6910625,

                "lon": -95.2091006,
                "numScholarships": 9
                },

                {
                "name": "Pearland, TX",

                "lat": 29.5635666,

                "lon": -95.2860474,
                "numScholarships": 3
                },

                {
                "name": "Plano, TX",

                "lat": 33.0198431,

                "lon": -96.6988856,
                "numScholarships": 3
                },

                {
                "name": "Plainview, TX",

                "lat": 34.1847936,

                "lon": -101.7068417,
                "numScholarships": 3
                },

                {
                "name": "Port Arthur, TX",

                "lat": 29.8849504,

                "lon": -93.93994699999999,
                "numScholarships": 3
                },

                {
                "name": "Ranger, TX",

                "lat": 32.4698522,

                "lon": -98.6789477,
                "numScholarships": 3
                },

                {
                "name": "Richardson, TX",

                "lat": 32.9483335,

                "lon": -96.7298519,
                "numScholarships": 15
                },

                {
                "name": "Round Rock, TX",

                "lat": 30.5082551,

                "lon": -97.678896,
                "numScholarships": 6
                },

                {
                "name": "San Antonio, TX",

                "lat": 29.4251905,

                "lon": -98.4945922,
                "numScholarships": 138
                },

                {
                "name": "San Benito, TX",

                "lat": 26.132576,

                "lon": -97.6311006,
                "numScholarships": 3
                },

                {
                "name": "San Angelo, TX",

                "lat": 31.4637723,

                "lon": -100.4370375,
                "numScholarships": 6
                },

                {
                "name": "San Marcos, TX",

                "lat": 29.8832749,

                "lon": -97.9413941,
                "numScholarships": 6
                },

                {
                "name": "Seguin, TX",

                "lat": 29.56892479999999,

                "lon": -97.96458319999999,
                "numScholarships": 6
                },

                {
                "name": "Sherman, TX",

                "lat": 33.6356618,

                "lon": -96.6088805,
                "numScholarships": 6
                },

                {
                "name": "Silsbee, TX",

                "lat": 30.3490976,

                "lon": -94.17796240000001,
                "numScholarships": 3
                },

                {
                "name": "Stephenville, TX",

                "lat": 32.2206958,

                "lon": -98.2022633,
                "numScholarships": 3
                },

                {
                "name": "Snyder, TX",

                "lat": 32.7178862,

                "lon": -100.9176184,
                "numScholarships": 3
                },

                {
                "name": "Southlake, TX",

                "lat": 32.9412363,

                "lon": -97.1341783,
                "numScholarships": 3
                },

                {
                "name": "Stafford, TX",

                "lat": 29.6160671,

                "lon": -95.55772209999999,
                "numScholarships": 9
                },

                {
                "name": "Temple, TX",

                "lat": 31.0982344,

                "lon": -97.342782,
                "numScholarships": 6
                },

                {
                "name": "The Woodlands, TX",

                "lat": 30.1657654,

                "lon": -95.46113199999999,
                "numScholarships": 3
                },

                {
                "name": "Tyler, TX",

                "lat": 32.3512601,

                "lon": -95.30106239999999,
                "numScholarships": 9
                },

                {
                "name": "Terrell, TX",

                "lat": 32.7359626,

                "lon": -96.2752569,
                "numScholarships": 3
                },

                {
                "name": "Texarkana, TX",

                "lat": 33.425125,

                "lon": -94.04768820000001,
                "numScholarships": 12
                },

                {
                "name": "Texas City, TX",

                "lat": 29.383845,

                "lon": -94.9027002,
                "numScholarships": 3
                },

                {
                "name": "Universal City, TX",

                "lat": 29.5522617,

                "lon": -98.30435469999999,
                "numScholarships": 3
                },

                {
                "name": "Weslaco, TX",

                "lat": 26.1595194,

                "lon": -97.9908366,
                "numScholarships": 9
                },

                {
                "name": "Waco, TX",

                "lat": 31.549333,

                "lon": -97.1466695,
                "numScholarships": 12
                },

                {
                "name": "Victoria, TX",

                "lat": 28.8052674,

                "lon": -97.0035982,
                "numScholarships": 12
                },

                {
                "name": "Vernon, TX",

                "lat": 34.1545306,

                "lon": -99.2650804,
                "numScholarships": 3
                },

                {
                "name": "Uvalde, TX",

                "lat": 29.20968359999999,

                "lon": -99.7861679,
                "numScholarships": 6
                },

                {
                "name": "Waxahachie, TX",

                "lat": 32.3865312,

                "lon": -96.8483311,
                "numScholarships": 3
                },

                {
                "name": "Weatherford, TX",

                "lat": 32.7592955,

                "lon": -97.7972544,
                "numScholarships": 3
                },

                {
                "name": "Webster, TX",

                "lat": 29.5377315,

                "lon": -95.1182645,
                "numScholarships": 6
                },

                {
                "name": "Abilene, TX",

                "lat": 32.4487364,

                "lon": -99.73314390000002,
                "numScholarships": 15
                },

                {
                "name": "Wichita Falls, TX",

                "lat": 33.9137085,

                "lon": -98.4933873,
                "numScholarships": 3
                },

                {
                "name": "Wharton, TX",

                "lat": 29.3116366,

                "lon": -96.1027371,
                "numScholarships": 3
                },

                {
                "name": "Amarillo, TX",

                "lat": 35.2219971,

                "lon": -101.8312969,
                "numScholarships": 12
                },

                {
                "name": "Arlington, TX",

                "lat": 32.735687,

                "lon": -97.10806559999999,
                "numScholarships": 24
                },

                {
                "name": "Austin, TX",

                "lat": 30.267153,

                "lon": -97.7430608,
                "numScholarships": 81
                },

                {
                "name": "Brownsville, TX",

                "lat": 25.9017472,

                "lon": -97.4974838,
                "numScholarships": 15
                },

                {
                "name": "Bryan, TX",

                "lat": 30.6743643,

                "lon": -96.3699632,
                "numScholarships": 9
                },

                {
                "name": "Carrollton, TX",

                "lat": 32.9756415,

                "lon": -96.8899636,
                "numScholarships": 3
                },

                {
                "name": "Corpus Christi, TX",

                "lat": 27.8005828,

                "lon": -97.39638099999999,
                "numScholarships": 15
                },

                {
                "name": "Dallas, TX",

                "lat": 32.7766642,

                "lon": -96.79698789999999,
                "numScholarships": 96
                }
            ],
            "collegeTuition": [
                {
                "tuition": "<= 2000",
                "count": 8
                },
                {
                "tuition": "2001-3000",
                "count": 33
                },
                {
                "tuition": "3001-4000",
                "count": 17
                },
                {
                "tuition": "4001-5000",
                "count": 3
                },
                {
                "tuition": "5001-6000",
                "count": 1
                },
                {
                "tuition": "6001-7000",
                "count": 6
                },
                {
                "tuition": "7001-8000",
                "count": 8
                },
                {
                "tuition": "8001-9000",
                "count": 9
                },
                {
                "tuition": "9001-10000",
                "count": 15
                },
                {
                "tuition": "10001-15000",
                "count": 31
                },
                {
                "tuition": "15001-20000",
                "count": 27
                },
                {
                "tuition": "20001-30000",
                "count": 7
                },
                {
                "tuition": "30001-40000",
                "count": 15
                },
                {
                "tuition": "40001+",
                "count": 9
                }
            ]
        }
    )


    @app.errorhandler(404)
    def resource_not_found(e):
        return jsonify(error=str(e)), 404

    @app.errorhandler(500)
    def internal_server_error(e):
        return jsonify(error=str(e)), 500

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/")
    def home():
        return "Hello I Am Here!"


app = create_app()

logging.basicConfig(level=logging.DEBUG)




# Run Application
if __name__ == '__main__':
    app.run(debug=True, port=5000)  # Set debug=True for development, change port if needed